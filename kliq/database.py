from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base

# engine = create_engine('sqlite:////db/kliq_db.db', convert_unicode=True, echo=True)
engine = create_engine('postgresql+psycopg2://kliquser:kliqlite123@localhost:5432/kliq')
db_session = scoped_session(sessionmaker(autocommit=False, autoflush=False, bind=engine))
Base = declarative_base()
Base.query = db_session.query_property()


def init_db():
    import kliq.models
    Base.metadata.create_all(bind=engine)
