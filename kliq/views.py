import re
import os
from urllib.request import urlretrieve
from flask import render_template, request, flash, redirect, url_for, make_response
from flask_login import login_user, logout_user, login_required, current_user
from sqlalchemy import and_, or_, func
from kliq.models import User, db, Message, MessageThread, Pictures, PictureLikes, PictureComments, Likes, \
    Location, ReportComment, Blacklist, ProfileVisits, States, Profiles, Interests, UserInterests
from kliq.forms import SignupForm, LoginForm, AvatarForm, PictureForm, CommentForm,\
    ReportForm, ChangePassword, CompleteLoginForm, ForgotPasswordForm, ProfileForm
from kliq.util.image_util import file_is_image, compress_image
from kliq.util.helpers import validphone, randompwd
from kliq.util.notifications import send_email, push_notifications, app_notifications
from kliq import lm, app
from werkzeug.utils import secure_filename
from geopy.geocoders import Nominatim
from authomatic.adapters import WerkzeugAdapter
from authomatic import Authomatic
from datetime import date


authomatic = Authomatic(app.config['AUTH_CONFIG'], 'secret8367457578something', report_errors=False)

@lm.user_loader
def load_user(userid):
    return User.query.filter(User.id == userid).first_or_404()


def load_user_via_urid(urid):
    return User.query.filter(User.urid == urid).first_or_404()


def load_msgthrd_urid(urid):
    return MessageThread.query.filter(MessageThread.urid == urid).first_or_404()


def load_user_by_phone(username):
    phone = validphone(username)
    return User.query.filter(User.phone_number == phone).first()


def load_img_urid(urid):
    return Pictures.query.filter(Pictures.urid == urid).first_or_404()


@app.route('/', methods=['GET', 'POST'])
@app.route('/index', methods=['GET', 'POST'])
def index():
    if current_user.is_authenticated:
        return redirect(url_for('home'))

    form = LoginForm(request.form)

    if request.method == 'POST' and form.validate():
        username = form.username.data

        if re.match(r"(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)", username):
            user = User.query.filter_by(email=username).first()
        else:
            user = load_user_by_phone(username)

        if user is None:
            flash('Invalid login credentials', 'error')
            # return redirect(url_for('login'))
        else:
            if user.is_correct_password(form.password.data):
                login_user(user)
                # update user
                user.online = 0
                user.last_visited = func.now()
                db.session.add(user)
                db.session.commit()
                return redirect(url_for('home'))
            else:
                flash('Invalid login credentials', 'error')

    return render_template('pages/index.html', form=form)


@app.route('/home')
@login_required
def home():
    # TODO ADD SEARCH CRITERIA
    user = User.query.filter(and_(User.id != current_user.id,
                                   ~User.id.in_(User.query.with_entities(User.id).filter(
                                       and_(Likes.liker_id == current_user.id, Likes.user_id == User.id))))).first()
    return render_template('pages/home.html', user=user)


@app.route('/kliq/<userid>/<action>', methods=['GET'])
@login_required
def kliq(userid, action):
    if action == '1' or action == '0':
        like = Likes(current_user.id, userid, action)
        db.session.add(like)
        db.session.commit()
        # if user liked the kliq-user, send push notification
        if action == '1':
            #load user
            user = load_user(userid)

            msg = current_user.full_name + " liked you."
            goto = url_for('user_details', userid=current_user.urid)
            push_notifications(msg, user.id, goto)
            send_email('Someone Liked You', user.email, render_template("emails/new-likes.html",
                               user=user))
            app_notifications(user.urid, 'like')

            # check if it is a mutual like
            check = Likes.query.filter_by(liker_id=userid, user_id=current_user.id, like_type=1).one_or_none()
            if check:
                msg = "You have a new mutual like"
                goto = url_for('mutual_likes')
                push_notifications(msg, userid, goto)
                send_email('You have a new mutual like', user.email, render_template("emails/mutual-likes.html",
                               user=user, sender=current_user))


    # TODO ADD SEARCH CRITERIA
    user = User.query.filter(and_(User.id != current_user.id,
                                   ~User.id.in_(User.query.with_entities(User.id).filter(
                                       and_(Likes.liker_id == current_user.id, Likes.user_id == User.id))),
                                   User.id > userid)).first()
    if user is None:
        # TODO figure out whether to show an error message or redirect to a different page
        return redirect(url_for('messages'))
    return render_template('pages/home.html', user=user)


@app.route('/mutual_likes')
@login_required
def mutual_likes():
    # check for unseen likes and update accordingly
    db.session.query(Likes).filter(Likes.user_id == current_user.id, Likes.seen == 0)\
        .update({'seen': 1}, synchronize_session=False)
    db.session.commit()

    users = User.query.filter(
        and_(and_(Likes.liker_id == current_user.id, Likes.user_id == User.id),
        and_(Likes.liker_id == User.id, Likes.user_id == current_user.id)
        )
    )
    return render_template('pages/mutual.html', mutual=users)


@app.route('/people_near_you', methods=['GET', 'POST'])
@login_required
def nearby():
    if current_user.my_location:
        users = User.query.join(User.my_location, aliased=True).filter(User.id != current_user.id,
                    Location.state == current_user.my_location.state,
                    Location.country == current_user.my_location.country)

        # just in case no users in location
        if users.count() == 0:
            users = User.query.join(User.my_location, aliased=True).filter(User.id != current_user.id,
                    Location.country == current_user.my_location.country)

        # in case nobody in entire nation
        if users.count() == 0:
            users = User.query.filter(User.id != current_user.id)
    else:
        users = User.query.filter(User.id != current_user.id)

    return render_template('pages/nearyou.html', users=users)


@app.route('/messages')
@login_required
def messages():
    thrds = MessageThread.query.filter(or_(MessageThread.sender_id == current_user.id,
                                           MessageThread.receiver_id == current_user.id))
    unread = MessageThread.query.filter(MessageThread.threads.any(and_(Message.receiver_id == current_user.id,
                                                                  Message.read_status == 0)))
    online = MessageThread.query.filter(or_(
        and_(MessageThread.sender_id == current_user.id, MessageThread.receiver.has(User.online == 0)),
        and_(MessageThread.receiver_id == current_user.id, MessageThread.sender.has(User.online == 0))
    ))

    return render_template('pages/messaging.html', thrds=thrds, unread=unread, online=online)


@app.route('/message_thread/<msgid>', methods=['GET', 'POST'])
@login_required
def message_thread(msgid):
    thread = load_msgthrd_urid(msgid)

    # check for unread messages and update accordingly
    db.session.query(Message).filter(Message.receiver_id == current_user.id, Message.thread_id == thread.id,
                                     Message.read_status == 0).update({'read_status': 1, 'date_read': func.now()},
                                                                      synchronize_session=False)
    db.session.commit()

    # get receipient
    if thread.sender_id == current_user.id:
        userid = thread.receiver_id
    else:
        userid = thread.sender_id
    user = User.query.get(userid)

    if request.method == 'POST':
        # insert message
        msg = Message(request.form['message'], request.form['thread'], current_user.id, request.form['rec'])
        db.session.add(msg)
        db.session.commit()

        # send push notification
        msg = current_user.full_name + " sent you a message"
        goto = url_for('message_thread', msgid=msgid)
        push_notifications(msg, request.form['rec'], goto)
        send_email('New Message', user.email, render_template("emails/message.html",
                               user=user, sender=current_user))
        app_notifications(user.urid, 'msg')

    return render_template('pages/message_thread.html', msgs=thread, user=user)


@app.route('/send_message/<userid>', methods=['GET', 'POST'])
@login_required
def send_message(userid):
    if current_user.urid == userid:
        return redirect(url_for('messages'))

    user = load_user_via_urid(userid)

    # check if users have initiated communication before
    # and redirect accordingly
    thread = MessageThread.query.filter(or_(and_(MessageThread.sender_id == current_user.id,
                                                 MessageThread.receiver_id == user.id),
                                            and_(MessageThread.sender_id == user.id,
                                                 MessageThread.receiver_id == current_user.id)
                                            )
                                        ).one_or_none()
    if thread is None:
        thread = MessageThread(current_user.id, user.id)
        db.session.add(thread)
        db.session.commit()

    return redirect(url_for('message_thread', msgid=thread.urid))


@app.route('/profile/<userid>')
@app.route('/user-details/<userid>')
@login_required
def user_details(userid):
    user = load_user_via_urid(userid)

    if user.id == current_user.id:
        return redirect(url_for('myprofile'))
    # update profile visits
    p = ProfileVisits(current_user.id, user.id)
    db.session.add(p)
    db.session.commit()

    # send push notification
    msg = current_user.full_name + " visited your profile"
    goto = url_for('user_details', userid=current_user.urid)
    push_notifications(msg, user.id, goto)
    send_email('Someone visited your profile', user.email, render_template("emails/profile-visit.html",
               user=user, sender=current_user))
    app_notifications(user.urid, 'visit')

    return render_template('pages/user-details.html', user=user)


@app.route('/profile_visits')
@login_required
def profile_visitors():
    # check for unseen likes and update accordingly
    db.session.query(ProfileVisits).filter(ProfileVisits.visited == current_user.id, ProfileVisits.seen == 0)\
        .update({'seen': 1}, synchronize_session=False)
    db.session.commit()

    pvisits = db.session.query(ProfileVisits).filter(ProfileVisits.visited==current_user.id)\
        .group_by(ProfileVisits.user_id).order_by(ProfileVisits.date_visited.desc()).all()

    return render_template('pages/profile_visits.html', pvisits=pvisits)


@app.route('/my-profile')
@login_required
def myprofile():
    return render_template('pages/user-details.html', user=current_user)


@app.route('/photos/<userid>')
@login_required
def photos(userid):
    user = load_user_via_urid(userid)

    return render_template('pages/photos.html', user=user)


@app.route('/myphotos')
@login_required
def myphotos():
    return render_template('pages/myphotos.html')


@app.route('/upload_picture', methods=['GET', 'POST'])
@login_required
def upload_picture():
    form = PictureForm(request.form)

    if request.method == 'POST':
        file = request.files['file']
        if file and file_is_image(file.filename):
            album = os.path.join(app.static_folder, app.config['UPLOAD_FOLDER'], app.config['IMG_UPLOAD_FOLDER'], current_user.urid)
            # check if user folder exists
            if not os.path.exists(album):
                os.makedirs(album)

            filename = secure_filename(file.filename).lower()
            file.save(os.path.join(album, filename))
            retval = compress_image(os.path.join(album, filename))

            if retval:
                # create pic
                pic = Pictures(filename, form.description.data, current_user.id)
                db.session.add(pic)
                db.session.commit()
                flash('Your picture has been uploaded', 'success')
                return redirect(url_for('view_picture', picid=pic.urid))
            else:
                flash('Something went wrong. Please try again', 'error')
        else:
            flash('Invalid file format')

    return render_template('pages/upload_pic.html', form=form)


@app.route('/view_picture/<picid>', methods=['GET', 'POST'])
@login_required
def view_picture(picid):
    picture = load_img_urid(picid)
    form = CommentForm(request.form)
    # check if user has liked picture
    like = PictureLikes.query.filter(and_(PictureLikes.liker_id == current_user.id,
                                          PictureLikes.picture_id == picture.id)).count()
    if request.method == 'POST' and form.validate():
        comment = PictureComments(picture.id, current_user.id, form.comment.data)
        db.session.add(comment)
        db.session.commit()
        flash('Your comment has been added', 'success')

        if current_user.id != picture.owner_id:
            # send push notification
            msg = current_user.full_name + " commented on your picture"
            goto = url_for('view_picture', picid=picid)
            push_notifications(msg, picture.owner_id, goto)
            send_email('Someone commented on your picture', picture.owner.email, render_template("emails/picture-comment.html",
                               user=picture.owner, pic=picture, sender=current_user))

        return redirect(request.referrer)
    return render_template('pages/view_picture.html', pic=picture, form=form, like=like)


@app.route('/report_comment/<comment_id>', methods=['GET', 'POST'])
@login_required
def report_comment(comment_id):
    com = PictureComments.query.get(comment_id)
    form = ReportForm(request.form)

    if request.method == 'POST' and form.validate():
        report = ReportComment(current_user.id, com.id, form.report.data)
        db.session.add(report)
        db.session.commit()
        flash('The comment has been reported', 'error')
        return redirect(url_for('view_picture', picid=com.picture.urid))
    return render_template('pages/report_comment.html', com=com, form=form)


@app.route('/delete_comment/<comment_id>', methods=['GET'])
@login_required
def delete_comment(comment_id):
    comment = PictureComments.query.get(comment_id)

    if comment.user_id == current_user.id or comment.picture.owner_id == current_user.id:
        comment.visible = 1
        db.session.add(comment)
        db.session.commit()
    return redirect(request.referrer)


@app.route('/block/<userid>', methods=['GET'])
@login_required
def block(userid):
    user = load_user_via_urid(userid)
    # check if already blocked
    blk = Blacklist.query.filter_by(user_id=current_user.id, blocked_id=user.id).one_or_none()
    if blk is None:
        blk = Blacklist(current_user.id, user.id)
        db.session.add(blk)
        db.session.commit()
    flash('The user has been blocked', 'success')
    return redirect(request.referrer)


@app.route('/unblock_users', methods=['GET'])
@login_required
def unblock_users():
    return render_template('pages/unblock.html')


@app.route('/unblock/<userid>', methods=['GET'])
@login_required
def unblock(userid):
    user = load_user_via_urid(userid)
    blk = Blacklist.query.filter_by(user_id=current_user.id, blocked_id=user.id).one_or_none()
    db.session.delete(blk)
    db.session.commit()
    flash('The user has been unblocked', 'success')
    return redirect(request.referrer)


@app.route('/like_picture/<urid>/<picid>/<action>', methods=['GET'])
@login_required
def like_picture(urid, picid, action):
    # check if user has already liked
    like = PictureLikes.query.filter(and_(PictureLikes.liker_id == current_user.id,
                                          PictureLikes.picture_id == picid)).count()
    # create sqlalchemy object
    piclike = PictureLikes(current_user.id, picid)
    # print(like)
    if like == 0:
        if action == '0':
            # insert
            db.session.add(piclike)
            db.session.commit()
            flash('You have liked this picture', 'success')
            if current_user.id != piclike.picture.owner_id:
                # send push notification
                msg = current_user.full_name + " liked your picture"
                goto = url_for('user_details', userid=current_user.urid)
                push_notifications(msg, piclike.picture.owner_id, goto)
                send_email('Someone Liked Your Picture', piclike.picture.owner.email, render_template("emails/picture-like.html",
                               user=piclike.picture.owner, pic=piclike.picture, sender=current_user))
        else:
            pass
    else:
        if action == '0':
            flash('You have already liked this picture', 'success')
        else:
            p = PictureLikes.query.filter(and_(PictureLikes.liker_id == current_user.id,
                                          PictureLikes.picture_id == picid)).first()
            # delete
            db.session.delete(p)
            db.session.commit()
            flash('You have unliked this picture', 'success')

    # redirect
    return redirect(url_for('view_picture', picid=urid))


@app.route('/upload_avatar', methods=['GET', 'POST'])
@login_required
def upload_avatar():
    form = AvatarForm(request.form)
    if request.method == 'POST':
        file = request.files['file']
        if file and file_is_image(file.filename):
            album = os.path.join(app.static_folder, app.config['UPLOAD_FOLDER'], app.config['IMG_UPLOAD_FOLDER'], current_user.urid)

            # check if user folder exists
            if not os.path.exists(album):
                os.makedirs(album)

            filename = secure_filename(file.filename).lower()
            file.save(os.path.join(album, filename))
            retval = compress_image(os.path.join(album, filename))

            if retval:
                # remove old avatar
                if current_user.avatar:
                    try:
                        os.remove(os.path.join(album, current_user.avatar))
                    except Exception as e:
                        print("Exception when removing old avatar: "+e)

                # update user avatar
                user = load_user(current_user.id)
                user.avatar = filename
                db.session.add(user)
                db.session.commit()
                return redirect(url_for('myprofile'))
            else:
                flash('Something went wrong. Please try again', 'error')
    return render_template('pages/upload_avatar.html', form=form)


@app.route('/settings', methods=['GET'])
@login_required
def settings():
    return render_template('pages/settings.html')


@app.route('/profile_edit', methods=['GET', 'POST'])
@login_required
def profile_edit():
    form = ProfileForm(request.form, obj=current_user.profile)
    form.state_of_origin.choices = [(a.state, a.state) for a in States.query.order_by('state')]

    interests = Interests.query.all()

    if request.method=='POST' and form.validate():
        model = Profiles.query.filter(Profiles.user_id == current_user.id).first()
        if not model:
            model = Profiles()
        model.user_id = current_user.id
        model.kids = form.kids.data
        model.smoking = form.smoking.data
        model.drinking = form.drinking.data
        model.occupation = form.occupation.data
        model.state_of_origin = form.state_of_origin.data
        model.complexion = form.complexion.data
        model.about_me = form.about_me.data
        model.highest_degree = form.highest_degree.data
        model.school = form.school.data

        db.session.add(model)
        db.session.commit()

        # remove & add interests
        UserInterests.query.filter_by(user_id=current_user.id).delete()
        db.session.commit()

        # add comments
        for interest in request.form.getlist('interests'):
            u = UserInterests()
            u.user_id = current_user.id
            u.interest_id = interest
            db.session.add(u)

        db.session.commit()

        flash('Your profile has been updated', 'success')

    myinterests = db.session.query(UserInterests.interest_id).filter(UserInterests.user_id==current_user.id).all()
    myinterests = [r[0] for r in myinterests]

    return render_template('pages/profile-edit.html', form=form, interests=interests, myinterests=myinterests)


@app.route('/change_password', methods=['GET', 'POST'])
@login_required
def change_password():
    form = ChangePassword(request.form)

    if request.method == 'POST' and form.validate():
        current_user.password = current_user.hash_password(form.password.data)
        db.session.add(current_user)
        db.session.commit()
        flash('Your password has been successfully changed', 'success')
        send_email('Password Changed', current_user.email, render_template("emails/password-change.html"))
        return redirect(url_for('settings'))
    return render_template('pages/change-password.html', form=form)


@app.route('/login', methods=['GET', 'POST'])
def login():
    return redirect(url_for('index'))
    # if current_user.is_authenticated:
    #     return redirect(url_for('home'))
    #
    # form = LoginForm(request.form)
    #
    # if request.method == 'POST' and form.validate():
    #     username = form.username.data
    #
    #     if re.match(r"(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)", username):
    #         user = User.query.filter_by(email=username).first()
    #     else:
    #         user = load_user_by_phone(username)
    #
    #     if user is None:
    #         flash('Invalid login credentials', 'error')
    #         return redirect(url_for('login'))
    #     if user.is_correct_password(form.password.data):
    #         login_user(user)
    #         # update user
    #         user.online = 0
    #         user.last_visited = func.now()
    #         db.session.add(user)
    #         db.session.commit()
    #         return redirect(url_for('home'))
    #     else:
    #         flash('Invalid login credentials', 'error')
    #
    # return render_template('pages/login.html', form=form)


@app.route('/social_login/<provider_name>/', methods=['GET', 'POST'])
def social_login(provider_name):
    if current_user.is_authenticated:
        return redirect(url_for('home'))
    # print('1')
    response = make_response()
    result = authomatic.login(adapter=WerkzeugAdapter(request, response), provider_name=provider_name)
    # print(response)
    if result:
        if result.user:
            result.user.update()
            # check the type of login
            if provider_name == 'facebook':
                # check if user exists
                user = User.query.filter_by(fb_auth_id=result.user.id).one_or_none()
                if user is None:
                    # check if user email already exists
                    user = User.query.filter_by(email=result.user.email).one_or_none()
                    if user is None:
                        # actual new user
                        user = User(result.user.name, None, result.user.gender, None, result.user.email, None, None)
                        user.fb_auth_id = result.user.id

                        # save avatar
                        album = os.path.join(app.static_folder, app.config['UPLOAD_FOLDER'],
                                             app.config['IMG_UPLOAD_FOLDER'], user.urid)
                        # check if user folder exists
                        if not os.path.exists(album):
                            os.makedirs(album)
                        urlretrieve(result.user.picture, os.path.join(album, "avatar.jpg"))
                        user.avatar = "avatar.jpg"

                        db.session.add(user)
                        db.session.commit()
                        return redirect(url_for('complete_login', provider_name='facebook', userid=user.urid))
                    else:
                        login_user(user)
                        # update user
                        user.online = 0
                        user.last_visited = func.now()
                        db.session.add(user)
                        db.session.commit()
                        return redirect(url_for('home'))
                else:
                    login_user(user)
                    # update user
                    user.online = 0
                    user.last_visited = func.now()
                    db.session.add(user)
                    db.session.commit()
                    return redirect(url_for('home'))
            elif provider_name == 'google':
                # check if user exists
                user = User.query.filter_by(google_auth_id=result.user.id).one_or_none()
                if user is None:
                    # check if user email already exists
                    user = User.query.filter_by(email=result.user.email).one_or_none()
                    if user is None:
                        # actual new user
                        user = User(result.user.name, None, result.user.gender, None, result.user.email, None, None)
                        user.google_auth_id = result.user.id

                        # save avatar
                        album = os.path.join(app.static_folder, app.config['UPLOAD_FOLDER'],
                                             app.config['IMG_UPLOAD_FOLDER'], user.urid)
                        # check if user folder exists
                        if not os.path.exists(album):
                            os.makedirs(album)
                        urlretrieve(result.user.picture[:-2] + '400', os.path.join(album, "avatar.jpg"))
                        user.avatar = "avatar.jpg"

                        db.session.add(user)
                        db.session.commit()
                        return redirect(url_for('complete_login', provider_name='google', userid=user.urid))
                    else:
                        login_user(user)
                        # update user
                        user.online = 0
                        user.last_visited = func.now()
                        db.session.add(user)
                        db.session.commit()
                        return redirect(url_for('home'))
                else:
                    login_user(user)
                    # update user
                    user.online = 0
                    user.last_visited = func.now()
                    db.session.add(user)
                    db.session.commit()
                    return redirect(url_for('home'))
        # return render_template('login.html', result=result)

    return response
    #flash('Something went wrong. Please retry')
    #return redirect(url_for('login'))

@app.route('/complete_login/<provider_name>/<userid>', methods=['GET', 'POST'])
def complete_login(provider_name, userid):
    if current_user.is_authenticated:
        return redirect(url_for('home'))
    user = load_user_via_urid(userid)
    form = CompleteLoginForm(request.form)

    if request.method == 'POST' and form.validate():
        if user.email is None:
            user.email = form.email.data
        user.date_of_birth = form.dob.data
        if user.gender is None:
            user.gender = form.gender.data
        user.interested_gender = form.int_gender.data
        # TODO add interested age range
        # generate random password
        pwd = randompwd()
        user.password = user.hash_password(pwd)
        login_user(user)
        send_email('Registration Complete', current_user.email, render_template("emails/complete-registration.html",
                                                                                password=pwd, user=user))
        # update user
        user.online = 0
        user.last_visited = func.now()
        db.session.add(user)
        db.session.commit()
        return redirect(url_for('home'))

    return render_template('pages/complete_login.html', form=form, user=user)

@app.route('/forgot_password', methods=['GET', 'POST'])
def forgotpassword():
    if current_user.is_authenticated:
        return redirect(url_for('home'))
    form = ForgotPasswordForm(request.form)
    if request.method == 'POST' and form.validate():
        user = User.query.filter_by(email=form.email.data).one_or_none()
        if user is None:
            flash('This email does not exist', 'error')
        else:
            pwd = randompwd()
            user.password = user.hash_password(pwd)
            db.session.add(user)
            db.session.commit()
            send_email('Password Reset', user.email, render_template("emails/password-reset.html",user=user,
                                                                     password=pwd))

            flash('Your password has been reset and sent to your email address', 'success')
    return render_template('pages/forgot_password.html', form=form)

@app.route('/signup', methods=['GET', 'POST'])
def signup():
    if current_user.is_authenticated:
        return redirect(url_for('home'))

    form = SignupForm(request.form)
    form.state.choices = [(a.state, a.state) for a in States.query.order_by('state')]
    if request.method == 'POST' and form.validate():
        user = User(form.fullname.data, form.dob.data, form.gender.data, form.int_gender.data, form.email.data,
                    form.phone.data, form.password.data)
        user.interested_in = form.interested_in.data
        loc = Location(user.id, None, None, None, form.state.data, form.country.data)
        user.my_location = loc
        db.session.add(user)
        db.session.commit()
        flash('You have been successfully registered', 'success')
        return redirect(url_for('login'))
    # print(form.errors)
    # return render_template('pages/signup.html', form=form)
    return render_template('pages/new-signup.html', form=form)


@app.route('/logout')
@login_required
def logout():
    if not current_user.is_authenticated:
        return redirect(url_for('home'))
    user = load_user(current_user.id)
    user.online = 1
    user.last_visited = func.now()
    db.session.add(user)
    db.session.commit()
    logout_user()
    flash('You have been logged out. Come back soon', 'success')
    return redirect(url_for('index'))


@app.route('/save_location', methods=['POST'])
@login_required
def save_location():
    if request.method == 'POST':
        lat = request.form['latitude']
        long = request.form['longitude']
        #get address
        geolocator = Nominatim()
        location = geolocator.reverse(lat+", "+long)
        print(lat+", "+long)
        print(location.address)
        address = location.address.split(',')
        print(address[-1].strip())
        loc = Location.query.filter_by(user_id=current_user.id).one_or_none()

        if loc is None:
            loca = Location(current_user.id, lat, long, address[2].strip(), address[3].strip(), address[-1].strip())
        else:
            loca = loc
            loca.latitude = lat
            loca.longitude = long
            loca.city = address[2].strip()
            loca.state = address[3].strip()
            loca.country = address[-1].strip()

        db.session.add(loca)
        db.session.commit()
        return make_response('success')
    return make_response('false')

@app.route('/unsubscribe/<userid>', methods=['GET', 'POST'])
def unsubscribe_email(userid):
    pass
