	angular.module('myApp.controllers', []);
	angular.module('myApp.models', []);
	var myApp = angular.module('myApp', ['ui.router', 'myApp.controllers', 'myApp.models']);

	myApp.config(['$interpolateProvider', '$locationProvider', function($interpolateProvider, $locationProvider) {
		$locationProvider.html5Mode(true)
		$interpolateProvider.startSymbol('[[')
		$interpolateProvider.endSymbol(']]')
	}])

	// configure our routes
	myApp.config(function($stateProvider, $urlRouterProvider) {
	  //
	  // For any unmatched url, redirect to /state1
	  $urlRouterProvider.otherwise("/");
	  //
	  // Now set up the states
	  $stateProvider
	    .state('address', {
	      url: "/address",
	      templateUrl: "partials/address.html",
	      controller: 'mainController'
	    })
	});