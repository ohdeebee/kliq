// This is the script that controls the side navigation
$(".openNav").click(function() {
  $("body").toggleClass("navOpen");
  $("nav").toggleClass("open");
  $(".message").toggleClass("open");
  $(".wrapper").toggleClass("open");
  $(this).toggleClass("open");
});


// This is the script for the full image animation on both login and signup pages
<<<<<<< HEAD
$(".login-wrap, .signup-wrap").backstretch(
    [
  "static/img/night-dark-blur-blurred.jpg",
  "static/img/road-street-blur-blurred.jpg"
    ],
    {duration: 10000, fade: 1000});
=======
$(".login-wrap, .signup-wrap").backstretch('/static/img/road-street-blur-blurred.jpg');
>>>>>>> c51c88dbec72eb139fd8fe896d7c215e1f0db678



$(".theBody").backstretch('static/img/love.jpeg');



// This is a plugin that allows you style your radio and checkbox input fields 
$(document).ready(function(){
  $('.custom-input').iCheck({
    radioClass: 'iradio_square-blue',
    increaseArea: '100%' // optional
  });
});

// This is a plugin that controls the image slider on the home page 
$(document).ready(function() {

  $("#image-preview").owlCarousel({

      navigation : false, // Show next and prev buttons
      slideSpeed : 300,
      paginationSpeed : 400,
      singleItem: true,
      responsive: true,
      pagination: true
  });

});

// This is a date picker plugin
$('.datepicker').datepicker();

//Like button toggle
$('.likeMe').click(function(){
    $('.likeMe').toggleClass('tada')
});

//Function to simulate a back button based on the users last web page
$(document).ready(function(){
	$('.back').click(function(){
		parent.history.back();
		return false;
	});
});

//Function that initialise the list scroll

var frame = new Sly('#frame', {
    slidee:     null,  // Selector, DOM element, or jQuery object with DOM element representing SLIDEE.
    horizontal: false, // Switch to horizontal mode.

    // Item based navigation
    itemNav:        null,  // Item navigation type. Can be: 'basic', 'centered', 'forceCentered'.
    itemSelector:   null,  // Select only items that match this selector.
    smart:          false, // Repositions the activated item to help with further navigation.
    activateOn:     null,  // Activate an item on this event. Can be: 'click', 'mouseenter', ...
    activateMiddle: false, // Always activate the item in the middle of the FRAME. forceCentered only.

    // Scrolling
    scrollSource: null,  // Element for catching the mouse wheel scrolling. Default is FRAME.
    scrollBy:     0,     // Pixels or items to move per one mouse scroll. 0 to disable scrolling.
    scrollHijack: 300,   // Milliseconds since last wheel event after which it is acceptable to hijack global scroll.
    scrollTrap:   false, // Don't bubble scrolling when hitting scrolling limits.

    // Dragging
    dragSource:    null,  // Selector or DOM element for catching dragging events. Default is FRAME.
    mouseDragging: false, // Enable navigation by dragging the SLIDEE with mouse cursor.
    touchDragging: false, // Enable navigation by dragging the SLIDEE with touch events.
    releaseSwing:  false, // Ease out on dragging swing release.
    swingSpeed:    0.2,   // Swing synchronization speed, where: 1 = instant, 0 = infinite.
    elasticBounds: false, // Stretch SLIDEE position limits when dragging past FRAME boundaries.
    interactive:   null,  // Selector for special interactive elements. Interactive elements don't initiate dragging. By default, Sly recognizes input, button, select, or a textarea elements as interactive. You can expand this pool by passing a selector into this option.

    // Scrollbar
    scrollBar:     null,  // Selector or DOM element for scrollbar container.
    dragHandle:    false, // Whether the scrollbar handle should be draggable.
    dynamicHandle: false, // Scrollbar handle represents the ratio between hidden and visible content.
    minHandleSize: 50,    // Minimal height or width (depends on sly direction) of a handle in pixels.
    clickBar:      false, // Enable navigation by clicking on scrollbar.
    syncSpeed:     0.5,   // Handle => SLIDEE synchronization speed, where: 1 = instant, 0 = infinite.

    // Pagesbar
    pagesBar:       null, // Selector or DOM element for pages bar container.
    activatePageOn: null, // Event used to activate page. Can be: click, mouseenter, ...
    pageBuilder:          // Page item generator.
        function (index) {
            return '<li>' + (index + 1) + '</li>';
        },

    // Navigation buttons
    forward:  null, // Selector or DOM element for "forward movement" button.
    backward: null, // Selector or DOM element for "backward movement" button.
    prev:     null, // Selector or DOM element for "previous item" button.
    next:     null, // Selector or DOM element for "next item" button.
    prevPage: null, // Selector or DOM element for "previous page" button.
    nextPage: null, // Selector or DOM element for "next page" button.

    // Automated cycling
    cycleBy:       null,  // Enable automatic cycling by 'items' or 'pages'.
    cycleInterval: 5000,  // Delay between cycles in milliseconds.
    pauseOnHover:  false, // Pause cycling when mouse hovers over the FRAME.
    startPaused:   false, // Whether to start in paused sate.

    // Mixed options
    moveBy:        300,     // Speed in pixels per second used by forward and backward buttons.
    speed:         0,       // Animations speed in milliseconds. 0 to disable animations.
    easing:        'swing', // Easing for duration based (tweening) animations.
    startAt:       null,    // Starting offset in pixels or items.
    keyboardNavBy: null,    // Enable keyboard navigation by 'items' or 'pages'.

    // Classes
    draggedClass:  'dragged', // Class for dragged elements (like SLIDEE or scrollbar handle).
    activeClass:   'active',  // Class for active items and pages.
    disabledClass: 'disabled' // Class for disabled navigation elements.
});



// var frame = new Sly('#frame', options).init();
// $frame.sly(options);


var $frame  = $('.img-preview');
var $wrap   = $frame.parent();
var options = {
    horizontal: 1,
    itemNav: 'forceCentered',
    smart: 1,
    activateMiddle: 1,
    mouseDragging: 1,
    touchDragging: 1,
    releaseSwing: 1,
    startAt: 0,
    elasticBounds: 1,
    easing: 'easeOutExpo',
    dragHandle: 1,
    dynamicHandle: 1,
};

var $frame  = $('.scrolling');
  var $wrap   = $frame.parent();
  var options = {
      horizontal: false,
      itemNav: 'basic',
      smart: true,
      releaseSwing: true,
      speed: 300,
      mouseDragging: 1,
      touchDragging: 1,
      scrollBar: '.scrollbar',
      elasticBounds: true,
      scrollTrap: true,
      dragHandle: true,
      dynamicHandle: false,
  };
//Function for uploading and croping image

$(document).ready(function() {
  $.uploadPreview({
    input_field: "#image-upload",
    preview_box: "#preview",
    label_field: "#image-label"
  });
});

//Function for hiding flash messages after 4 seconds    

$(".flashes").show();
setTimeout(function() { $(".flashes").hide(); }, 4000);


//Function to animate the exit of a div using animate.css



$('.exit').on('click',function(){
    $(this).parent('div.nearYou-wrapper').addClass('bounceOut');
});


//Function to add and remove loding page

$(window).load(function() {
    // Animate loader off screen
    $(".loading-animation").addClass("hide");
  });


//Function for form animation

$('.form-control').on('focus blur', function (e) {
    $(this).parents('.form-group').toggleClass('focused', (e.type === 'focus' || this.value.length > 0));
}).trigger('blur');

//Function for signup page form animation
$(window).load(function(){  
  $(".done").click(function(){
    var this_li_ind = $(this).parent().parent("li").index();
    if($('.payment-wizard li').hasClass("jump-here")){
      $(this).parent().parent("li").removeClass("active").addClass("completed");
      $(this).parent(".wizard-content").slideUp();
      $('.payment-wizard li.jump-here').removeClass("jump-here");
    }else{
      $(this).parent().parent("li").removeClass("active").addClass("completed");
      $(this).parent(".wizard-content").slideUp();
      $(this).parent().parent("li").next("li:not('.completed')").addClass('active').children('.wizard-content').slideDown();
    }
  });
  
  $('.payment-wizard li .wizard-heading').click(function(){
    if($(this).parent().hasClass('completed')){
      var this_li_ind = $(this).parent("li").index();   
      var li_ind = $('.payment-wizard li.active').index();
      if(this_li_ind < li_ind){
        $('.payment-wizard li.active').addClass("jump-here");
      }
      $(this).parent().addClass('active').removeClass('completed');
      $(this).siblings('.wizard-content').slideDown();
    }
  });
})

<<<<<<< HEAD
$(function() {
      $(".dial").knob();
  });
=======

//Function for Profile Complition chat
$(function() {
      $(".dial").knob({
        readOnly: true
      });
  });


$(document).ready(function(){
  $('.chat-input').emojiarea({
    wysiwyg: true,
    button: '.emoji',
    buttonPosition: 'before'
  });
})
>>>>>>> c51c88dbec72eb139fd8fe896d7c215e1f0db678
