from sqlalchemy import Column, Integer, String, Date, DateTime, func, ForeignKey, and_, Binary
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.orm import relationship
from flask_login import UserMixin
from kliq.util.helpers import genID, validphone
from datetime import date, timedelta
from kliq import bcrypt
from geopy.distance import vincenty
from math import floor
import time

db = SQLAlchemy()

class States(db.Model):
    __tablename__ = 'states'

    id = Column(Integer, primary_key=True)
    state = Column(String(30))

class User(UserMixin, db.Model):
    __tablename__ = 'users'

    id = Column(Integer, primary_key=True)
    urid = Column(String(200), unique=True)
    full_name = Column(String(100))
    date_of_birth = Column(Date)
    avatar = Column(String(100))
    email = Column(String(120))
    gender = Column(String(6))
    google_auth_id = Column(String(200))
    fb_auth_id = Column(String(200))
    interested_gender = Column(String(6))
    interested_age_min = Column(String(6))
    interested_age_max = Column(String(6))
    interested_location = Column(String(150))
    interested_in = Column(String(50))
    phone_number = Column(String(20))
    password = Column(String(200))
    sign_up_date = Column(DateTime, default=func.now())
    last_visited = Column(DateTime)
    online = Column(Integer)

    outbox = relationship('MessageThread', foreign_keys='MessageThread.sender_id', back_populates='sender')
    inbox = relationship('MessageThread', foreign_keys='MessageThread.receiver_id', back_populates='receiver')

    # for notifications
    unread = relationship('Message', primaryjoin='and_(Message.receiver_id==User.id, Message.read_status==0)')
    new_likes = relationship('Likes', primaryjoin='and_(Likes.user_id==User.id, Likes.seen==0)')
    new_visits = relationship('ProfileVisits', primaryjoin='and_(ProfileVisits.visited==User.id, ProfileVisits.seen==0)')

    photos = relationship('Pictures', foreign_keys='Pictures.owner_id', back_populates='owner')
    photolikes = relationship('PictureLikes', foreign_keys='PictureLikes.liker_id', back_populates='liker')
    photo_comments = relationship('PictureComments', foreign_keys='PictureComments.user_id', back_populates='commentor')

    likes = relationship('Likes', foreign_keys='Likes.liker_id', back_populates='liker')
    liked_by = relationship('Likes', foreign_keys='Likes.user_id', back_populates='user')


    my_location = relationship('Location', foreign_keys='Location.user_id', back_populates='owner', uselist=False)
    blockedlist = relationship('Blacklist', foreign_keys='Blacklist.user_id', back_populates='owner', lazy='dynamic')

    visits = relationship('ProfileVisits', foreign_keys='ProfileVisits.user_id', back_populates='guest')
    visited_by = relationship('ProfileVisits', foreign_keys='ProfileVisits.visited', back_populates='owner')

    profile = relationship('Profiles', foreign_keys='Profiles.user_id', back_populates='user', uselist=False)
    interests = relationship('UserInterests', foreign_keys='UserInterests.user_id', back_populates='user',
                             cascade="all, delete-orphan")

    def __init__(self, fullname, dob, gender, int_gender, email, phone, pwd):
        self.full_name = fullname
        self.urid = genID(User, User.urid)
        self.date_of_birth = dob
        self.gender = gender
        self.interested_gender = int_gender
        self.email = email
        if phone:
            self.phone_number = validphone(phone)
        if pwd:
            self.password = self.hash_password(pwd)

    def __repr__(self):
        return self.full_name

    def hash_password(self, pwd):
        return bcrypt.generate_password_hash(pwd).decode('utf8')

    def is_correct_password(self, pwd):
        if bcrypt.check_password_hash(self.password, pwd):
            return True
        return False

    def is_blocked(self, userid):
        if db.session.query(Blacklist).filter(and_(Blacklist.user_id==self.id, Blacklist.blocked_id==userid)).count() > 0:
            return True
        return False

    # properties for notifications
    @property
    def unread_count(self):
        return len(self.unread)

    @property
    def new_visits_count(self):
        return len(self.new_visits)

    @property
    def new_like_count(self):
        return len(self.new_likes)

    @property
    def photocount(self):
        return len(self.photos)

    @property
    def location(self):
        if self.my_location == [] or self.my_location is None:
            return None
        else:
            return self.my_location

    def get_id(self):
        return str(self.id)

    def proximity(self, coors):
        if not coors:
            return None
        elif not self.coordinates:
            return None
        else:
            return floor(vincenty(self.coordinates, coors).kilometers)

    def proximity_text(self, coors):
        prox = self.proximity(coors)
        if prox <= 1:
            return "Less than 1km away"
        elif prox <= 5:
            return "Less than 5km away"
        elif prox <= 20:
            return "Less than 20km away"
        else:
            return "%s km away" % prox

    @property
    def coordinates(self):
        if self.my_location:
            coor = (self.my_location.latitude, self.my_location.longitude)
            return coor
        else:
            return None

    @property
    def is_authenticated(self):
        return True

    @property
    def age(self):
        today = date.today()
        return today.year - self.date_of_birth.year - ((today.month, today.day) < (self.date_of_birth.month,
                                                                                   self.date_of_birth.day))

    def get_avatar(self):
        if self.avatar:
            return '/static/uploads/images/'+self.urid+'/'+self.avatar
        else:
            if self.gender == 'Male':
                return '/static/img/default_image_male.jpg'
            else:
                return '/static/img/female-user.jpg'

    def is_online(self):
        if self.online == 0:
            return True
        else:
            return False

    @property
    def is_active(self):
        return True

    @property
    def is_anonymous(self):
        return False

    @property
    def gender_pronoun(self):
        if self.gender == 'Male':
            return 'His'
        else:
            return 'Her'

    @property
    def completion(self):
        value = 0
        total = 22

        if self.avatar:
            value += 4
        if self.full_name:
            value += 2
        if self.date_of_birth:
            value += 2
        if self.gender:
            value += 1
        if self.profile.about_me:
            value += 3
        if self.profile.kids:
            value += 1
        if self.profile.smoking:
            value += 1
        if self.profile.drinking:
            value += 1
        if self.profile.occupation:
            value += 1
        if self.profile.state_of_origin:
            value += 1
        if self.profile.complexion:
            value += 1
        if self.profile.height_feet:
            value += 1
        if self.profile.height_inches:
            value += 1
        if self.profile.school:
            value += 1
        if self.profile.highest_degree:
            value += 1

        return round((value * 100)/total)


class Profiles(db.Model):
    __tablename__ = 'profiles'

    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey('users.id'))
    kids = Column(String(50))
    smoking = Column(String(50))
    drinking = Column(String(50))
    occupation = Column(String(200))
    state_of_origin = Column(String(50))
    complexion = Column(String(50))
    height_feet = Column(Integer)
    height_inches = Column(Integer)
    about_me = Column(String(4000))
    school = Column(String(100))
    highest_degree = Column(String(50))
    # interests = Column(String(4000))

    user = relationship('User', foreign_keys=user_id, back_populates='profile')


class ProfileVisits(db.Model):
    __tablename__ = 'profile_visits'

    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey('users.id'))
    visited = Column(Integer, ForeignKey('users.id'))
    date_visited = Column(DateTime, default=func.now())
    seen = Column(Integer, default=0)

    guest = relationship('User', foreign_keys=user_id, back_populates='visits', uselist=False)
    owner = relationship('User', foreign_keys=visited, back_populates='visited_by')

    def __init__(self, user_id, visited):
        self.user_id = user_id
        self.visited = visited

    def date_visited_text(self):
        # convert to unix timestamp
        current_date = time.mktime(date.today().timetuple())
        dt_visited = time.mktime(self.date_visited.timetuple())

        # get hours difference
        hr = float(current_date-dt_visited) / 3600

        if hr < 0.017:
            retval = "A few seconds ago"
        elif hr < 1:
            mins = int(hr*60)
            retval = "%s minutes ago" % mins
        else:
            hr = floor(hr)
            if hr == 1:
                retval = "An hour ago"
            elif hr < 24:
                retval = "%s hours ago" % hr
            else:
                if 24 <= hr < 48:
                    retval = "A day ago"
                elif hr < 168:
                    days = int(hr/24)
                    retval = "%s days ago" % days
                else:
                    if 168 <= hr < 336:
                        retval = "A week ago"
                    elif hr < 730:
                        weeks = int(hr/168)
                        retval = "%s weeks ago" % weeks
                    else:
                        if 730 <= hr < 1460:
                            retval = "A month ago"
                        elif hr < 8760:
                            months = int(hr/730)
                            retval = "%s months ago" % months
                        else:
                            if 8760 <= hr < 17520:
                                retval = "A year ago"
                            else:
                                years = int(hr/8760)
                                retval = "%s years ago" % years

        return retval

class Likes(db.Model):
    __tablename__ = 'likes'

    id = Column(Integer, primary_key=True)
    liker_id = Column(Integer, ForeignKey('users.id'))
    user_id = Column(Integer, ForeignKey('users.id'))
    # 1 == like | 0 == dislike
    like_type = Column(Integer)
    seen = Column(Integer, default=0)
    date_created = Column(DateTime, default=func.now())

    liker = relationship('User', primaryjoin='and_(Likes.liker_id==User.id, Likes.like_type==1)', back_populates='likes')
    disliker = relationship('User', primaryjoin='and_(Likes.liker_id==User.id, Likes.like_type==0)')
    user = relationship('User', foreign_keys=user_id, back_populates='liked_by')

    def __init__(self, liker, user_id, action):
        self.liker_id = liker
        self.user_id = user_id
        self.like_type = action

class Pictures(db.Model):
    __tablename__ = 'pictures'

    id = Column(Integer, primary_key=True)
    urid = Column(String(200), unique=True)
    owner_id = Column(Integer, ForeignKey('users.id'))
    filename = Column(String(200))
    description = Column(String(5000))
    date_uploaded = Column(DateTime, default=func.now())

    owner = relationship('User', foreign_keys=owner_id, back_populates='photos')
    likes = relationship('PictureLikes', foreign_keys='PictureLikes.picture_id', back_populates='picture')
    comments = relationship('PictureComments',
                            primaryjoin='and_(PictureComments.picture_id==Pictures.id, PictureComments.visible==0)',
                            back_populates='picture')

    @property
    def comment_count(self):
        return len(self.comments)

    @property
    def like_count(self):
        return len(self.likes)

    def __init__(self, filename, description, owner):
        self.filename = filename
        self.description = description
        self.urid = genID(Pictures, Pictures.urid)
        self.owner_id = owner


class PictureLikes(db.Model):
    __tablename__ = 'picture_likes'

    id = Column(Integer, primary_key=True)
    picture_id = Column(Integer, ForeignKey('pictures.id'))
    liker_id = Column(Integer, ForeignKey('users.id'))
    date_liked = Column(DateTime, default=func.now())

    picture = relationship('Pictures', foreign_keys=picture_id, back_populates='likes')
    liker = relationship('User', foreign_keys=liker_id, back_populates='photolikes')

    def __init__(self, liker_id, picture_id):
        self.liker_id = liker_id
        self.picture_id = picture_id


class PictureComments(db.Model):
    __tablename__ = 'picture_comments'

    id = Column(Integer, primary_key=True)
    urid = Column(String(200), unique=True)
    picture_id = Column(Integer, ForeignKey('pictures.id'))
    user_id = Column(Integer, ForeignKey('users.id'))
    comment = Column(String(5000))
    date_created = Column(DateTime, default=func.now())
    visible = Column(Integer, default=0)

    picture = relationship('Pictures', foreign_keys=picture_id, back_populates='comments')
    commentor = relationship('User', foreign_keys=user_id, back_populates='photo_comments')

    def __init__(self, picture_id, user_id, comment):
        self.picture_id = picture_id
        self.user_id = user_id
        self.comment = comment
        self.urid = genID(PictureComments, PictureComments.urid)


class MessageThread(db.Model):
    __tablename__ = 'message_threads'

    id = Column(Integer, primary_key=True)
    urid = Column(String(200), unique=True)
    sender_id = Column(Integer, ForeignKey('users.id'))
    receiver_id = Column(Integer, ForeignKey('users.id'))
    date_created = Column(DateTime, default=func.now())

    sender = relationship('User', foreign_keys=sender_id, back_populates='outbox')
    receiver = relationship('User', foreign_keys=receiver_id, back_populates='inbox')
    threads = relationship('Message', foreign_keys='Message.thread_id', back_populates='msg_thread',
                           order_by='Message.date_created')

    def __repr__(self):
        return '<MessageThreadId %r>' % self.id

    def __init__(self, sender, receiver):
        self.sender_id = sender
        self.receiver_id = receiver
        self.urid = genID(MessageThread, MessageThread.urid)


class Message(db.Model):
    __tablename__ = 'messages'

    id = Column(Integer, primary_key=True)
    urid = Column(String(200), unique=True)
    thread_id = Column(Integer, ForeignKey('message_threads.id'))
    sender_id = Column(Integer, ForeignKey('users.id'))
    receiver_id = Column(Integer, ForeignKey('users.id'))
    message = Column(String(4000))
    date_created = Column(DateTime, default=func.now())
    date_read = Column(DateTime)
    read_status = Column(Integer, default=0)
    sender_delete = Column(Integer)
    receiver_delete = Column(Integer)

    msg_thread = relationship('MessageThread', foreign_keys=thread_id, back_populates='threads')
    sender = relationship('User', foreign_keys=sender_id)
    receiver = relationship('User', foreign_keys=receiver_id)

    def __repr__(self):
        return self.message

    def __init__(self, message, thread, sender, receiver):
        self.message = message
        self.thread_id = thread
        self.sender_id = sender
        self.receiver_id = receiver
        self.urid = genID(Message, Message.urid)

    @property
    def is_read(self):
        if self.read_status == 1:
            return True
        return False


class Location(db.Model):
    __tablename__ = 'location'

    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey('users.id'), unique=True)
    longitude = Column(String(200))
    latitude = Column(String(200))
    city = Column(String(200))
    state = Column(String(200))
    country = Column(String(200))
    date_updated = Column(DateTime, default=func.now())

    owner = relationship('User', foreign_keys=user_id, back_populates='my_location')

    def __init__(self, user_id, longitude, latitude, city, state, country):
        self.user_id = user_id
        self.longitude = longitude
        self.latitude = latitude
        self.city = city
        self.state = state
        self.country = country

    def __repr__(self):
        return self.state + ', ' + self.country


class ReportComment(db.Model):
    __tablename__ = 'report_comment'

    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey('users.id'))
    comment_id = Column(Integer, ForeignKey('picture_comments.id'))
    report_text = Column(String(5000))
    date_reported = Column(DateTime, default=func.now())

    owner = relationship('User', foreign_keys=user_id)
    comment = relationship('PictureComments', foreign_keys=comment_id)

    def __init__(self, user_id, comment_id, report_text):
        self.user_id = user_id
        self.comment_id = comment_id
        self.report_text = report_text


class Blacklist(db.Model):
    __tablename__ = 'blacklist'

    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey('users.id'))
    blocked_id = Column(Integer, ForeignKey('users.id'))
    date_blocked = Column(DateTime, default=func.now())

    owner = relationship('User', foreign_keys=user_id, back_populates='blockedlist')
    blocked = relationship('User', foreign_keys=blocked_id)

    def __init__(self, user_id, blocked_id):
        self.user_id = user_id
        self.blocked_id = blocked_id


class Interests(db.Model):
    __tablename__ = 'interests'

    id = Column(Integer, primary_key=True)
    interest = Column(String(200))
    parent_id = Column(Integer, ForeignKey('interests.id'))
    picture = Column(String(200))

    parent = relationship('Interests', remote_side=[id])
    users = relationship('UserInterests', foreign_keys='UserInterests.interest_id', back_populates='interest')

    def __repr__(self):
        return self.interest

    @property
    def get_picture(self):
        if self.parent is None:
            return self.picture
        else:
            return self.parent.get_picture


class UserInterests(db.Model):
    __tablename__ = 'user_interests'

    id = Column(Integer, primary_key=True)
    user_id = Column(Integer, ForeignKey('users.id'))
    interest_id = Column(Integer, ForeignKey('interests.id'))

    interest = relationship('Interests', foreign_keys=interest_id, back_populates='users', uselist=False)
    user = relationship('User', foreign_keys=user_id, back_populates='interests', uselist=False)
