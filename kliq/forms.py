from wtforms import Form, RadioField, FileField, StringField, DateField, PasswordField, HiddenField,\
    TextAreaField, validators, SelectField
from kliq.util.validators import Unique, Numeric, ValidAge
from kliq.models import User, Profiles


class AvatarForm(Form):
    file = FileField('Select Image')


class InterestForm(Form):
    pass


class ForgotPasswordForm(Form):
    email = StringField('Enter Your Email Address', [validators.DataRequired(), validators.Length(min=6, max=120),
                                          validators.Email()])


class ChangePassword(Form):
    password = PasswordField('Password', [validators.DataRequired()])
    confirm_password = PasswordField('Confirm Password', [validators.DataRequired(),
                                                          validators.EqualTo('password', 'The passwords do not match')])

class ProfileForm(Form):
    employOptions = [('Student', 'Student'), ('Employed', 'Employed'), ('Unemployed', 'Unemployed')]
    kidOptions = [('No', 'No'), ('Yes', 'Yes'), ('Later', 'Later'), ('Never', 'Never')]
    smokeOptions = [('No', 'No'), ('Socially', 'Socially'), ('Yes', 'Yes')]
    complexionOptions = [('Fair', 'Fair'), ('Caramel', 'Caramel'), ('Dark', 'Dark')]
    degreeOptions = [('PhD', 'PhD'), ('MSc', 'MSc'), ('MBA', 'MBA'), ('BSc', 'BSc'), ('SSCE', 'SSCE')]

    about_me = TextAreaField('About Me')
    state_of_origin = SelectField('State of Origin')
    occupation = SelectField('Occupation', choices=employOptions)
    kids = SelectField('Kids', choices=kidOptions)
    smoking = SelectField('Smoking', choices=smokeOptions)
    drinking = SelectField('Drinking', choices=smokeOptions)
    complexion = SelectField('Complexion', choices=complexionOptions)
    # height = StringField('Height', [validators.DataRequired(), validators.Length(min=3, max=100)])
    height_feet = StringField('Height (Feet)', [Numeric(Profiles, Profiles.height_feet, message='Your height must be numeric')])
    height_inches = StringField('Height (Inches)', [Numeric(Profiles, Profiles.height_inches, message='Your height must be numeric')])
    # interests = TextAreaField('Interests')
    highest_degree = SelectField('Highest Degree', choices=degreeOptions, default='BSc')
    school = StringField('School')

class PictureForm(Form):
    file = FileField('Select Image')
    description = TextAreaField('Enter a description (Optional)')


class CommentForm(Form):
    comment = TextAreaField('Enter your comment', [validators.DataRequired()])


class LoginForm(Form):
    username = StringField('Email Address', [validators.DataRequired()])
    password = PasswordField('Password', [validators.DataRequired()])


class MessageForm(Form):
    body = TextAreaField('Message', [validators.DataRequired()])
    receiver = HiddenField()
    thread = HiddenField()


class ReportForm(Form):
    comment_id = HiddenField()
    report = TextAreaField('Report Comment', [validators.DataRequired()])


class SignupForm(Form):
    genderOptions = [('Male', 'Man'), ('Female', 'Woman')]
    intgenderOptions = [('Male', 'Men'), ('Female', 'Women')]
    intinOptions = [('Dating', 'Date'), ('Make friends', 'Make new friends'), ('Chat', 'Chat')]
    countryChoices = [('Nigeria', 'Nigeria'), ('Ghana', 'Ghana')]

    fullname = StringField('Full Name', [validators.DataRequired(), validators.Length(min=3, max=100)])
    dob = DateField('Date of Birth', [validators.DataRequired(), ValidAge(User, User.date_of_birth)], format='%m/%d/%Y')
    gender = SelectField('Gender', [validators.DataRequired()], choices=genderOptions, default='Male')
    int_gender = SelectField('Interested in', [validators.DataRequired()], choices=intgenderOptions, default='Female')
    interested_in = SelectField('Here to',  choices=intinOptions)
    state = SelectField('State')
    country = SelectField('Country', choices=countryChoices)
    email = StringField('Email Address', [validators.DataRequired(), validators.Length(min=6, max=120),
                                          validators.Email(),
                                          Unique(User, User.email, message='This email address already exists.')])
    # restrict to numbers only
    phone = StringField('Phone Number', [validators.DataRequired(), validators.Length(min=11, max=13),
                                         Numeric(User, User.phone_number, message='Your phone number must be numeric'),
                                         Unique(User, User.phone_number, message='This phone number already exists.')])
    password = PasswordField('Password', [validators.DataRequired()])

class CompleteLoginForm(Form):
    genderOptions = [('Male', 'Male'), ('Female', 'Female')]
    intgenderOptions = [('Male', 'Men'), ('Female', 'Women')]

    dob = DateField('Date of Birth', [validators.DataRequired(), ValidAge(User, User.date_of_birth)], format='%m/%d/%Y')
    gender = SelectField('Gender', [validators.DataRequired()], choices=genderOptions, default='Male')
    int_gender = SelectField('Interested in', [validators.DataRequired()], choices=intgenderOptions, default='Female')
    email = StringField('Email Address', [validators.DataRequired(), validators.Length(min=6, max=120),
                                          validators.Email(),
                                          Unique(User, User.email, message='This email address already exists.')])
    # restrict to numbers only
    phone = StringField('Phone Number', [validators.DataRequired(), validators.Length(min=11, max=13),
                                         Numeric(User, User.phone_number, message='Your phone number must be numeric'),
                                         Unique(User, User.phone_number, message='This phone number already exists.')])
