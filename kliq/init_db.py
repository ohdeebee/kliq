import sqlite3
from contextlib import closing

DATABASE = 'db/kliq_db.db'
SQLSCRIPT = 'db/schema.sql'

# connect to db
connection = sqlite3.connect(DATABASE)


try:
    # check if table exists
    curs = connection.cursor()
    curs.execute('select * from users')
    print('Users table already exists...')
except sqlite3.OperationalError:
    # create Users table
    with closing(connection) as db:
        with open(SQLSCRIPT, mode='r') as f:
            db.cursor().executescript(f.read())
            db.commit()
    print('Users table has been successfully created...')
