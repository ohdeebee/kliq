drop table if exists users;
create table users (
id integer primary key auto_increment,
urid varchar(50) unique,
full_name varchar(50) null,
date_of_birth date null,
avatar varchar(50) null,
email varchar(50) null,
gender varchar(50) null,
google_auth_id varchar(50) null,
fb_auth_id varchar(50) null,
interested_gender varchar(50) null,
interested_age_min varchar(50) null,
interested_age_max varchar(50) null,
interested_location varchar(50) null,
interested_in varchar(50) null,
phone_number varchar(50) null,
password varchar(50) null,
sign_up_date datetime,
last_visited datetime,
online integer
);

drop table if exists message_threads;
create table message_threads(
id integer primary key auto_increment,
urid varchar(50) unique,
sender_id INTEGER not null,
receiver_id INTEGER not null,
date_created datetime,
FOREIGN KEY(sender_id) REFERENCES users (id),
FOREIGN KEY(receiver_id) REFERENCES users (id)
);

drop table if exists messages;
create table messages (
id integer primary key auto_increment,
urid varchar(50) unique,
thread_id INTEGER not null,
sender_id INTEGER not null,
receiver_id INTEGER not null,
message text not null,
date_created datetime,
date_read datetime,
read_status integer,
sender_delete int,
receiver_delete int,
FOREIGN KEY(sender_id) REFERENCES users (id),
FOREIGN KEY(receiver_id) REFERENCES users (id),
FOREIGN KEY(thread_id) REFERENCES message_threads (id)
);

drop table if exists profiles;
create table profiles (
id integer primary key auto_increment,
urid varchar(50) unique,
user_id integer not null,
kids varchar(50),
smoking varchar(50),
drinking varchar(50),
occupation varchar(50),
state_of_origin varchar(50),
complexion varchar(50),
height_feet integer,
height_inches integer,
about_me text,
school varchar(50),
highest_degree varchar(50),
FOREIGN KEY(user_id) REFERENCES users (id)
);

drop table if exists interests;
create table interests (
id integer primary key auto_increment,
interest varchar(50),
parent_id integer,
picture varchar(50),
FOREIGN KEY(parent_id) REFERENCES interests (id)
);

drop table if exists user_interests;
create table user_interests (
id integer primary key auto_increment,
user_id integer,
interest_id integer,
FOREIGN KEY(user_id) REFERENCES users (id),
FOREIGN KEY(interest_id) REFERENCES interests (id)
);

drop table if exists states;
create table states (
id integer primary key auto_increment,
state varchar(50)
);

drop table if exists pictures;
create table pictures (
id integer primary key auto_increment,
urid varchar(50) unique,
owner_id INTEGER not null,
filename varchar(50),
description text,
date_uploaded datetime,
FOREIGN KEY(owner_id) REFERENCES users (id)
);

drop table if exists picture_likes;
create table picture_likes (
id integer primary key auto_increment,
picture_id INTEGER not null,
liker_id INTEGER not null,
date_liked datetime,
FOREIGN KEY(picture_id) REFERENCES pictures (id),
FOREIGN KEY(liker_id) REFERENCES users (id)
);

drop table if exists picture_comments;
create table picture_comments (
id integer primary key auto_increment,
urid varchar(50) unique,
picture_id INTEGER not null,
user_id INTEGER not null,
comment text(4000) not null,
date_created datetime,
visible integer default 0,
FOREIGN KEY(picture_id) REFERENCES pictures (id),
FOREIGN KEY(user_id) REFERENCES users (id)
);

drop table if exists likes;
create table likes(
id integer primary key auto_increment,
liker_id integer,
like_type integer,
user_id integer,
seen integer,
date_created datetime,
FOREIGN KEY (liker_id) REFERENCES users (id),
FOREIGN KEY (user_id) REFERENCES users (id)
);

drop table if exists location;
create table location(
id integer primary key auto_increment,
user_id integer,
longitude varchar(50),
latitude varchar(50),
city varchar(50),
state varchar(50),
country varchar(50),
date_updated datetime,
FOREIGN KEY (user_id) REFERENCES users (id)
);

drop table if exists blacklist;
create table blacklist(
id integer primary key auto_increment,
user_id integer,
blocked_id INTEGER,
date_blocked datetime,
FOREIGN KEY (user_id) REFERENCES users (id),
FOREIGN KEY (blocked_id) REFERENCES users (id)
);

drop table if exists profile_visits;
create table profile_visits(
id integer primary key auto_increment,
user_id integer,
visited INTEGER,
date_visited datetime,
seen integer default 0,
FOREIGN KEY (user_id) REFERENCES users (id),
FOREIGN KEY (visited) REFERENCES users (id)
);

drop table if exists report_comment;
create table report_comment(
id integer primary key auto_increment,
user_id integer,
comment_id INTEGER,
report_text text(4000),
date_reported datetime,
FOREIGN KEY (user_id) REFERENCES users (id),
FOREIGN KEY (comment_id) REFERENCES picture_comments (id)
);
