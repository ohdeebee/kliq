drop table if exists users;
create table users (
id serial primary key,
urid varchar(200) unique,
full_name varchar(200) null,
date_of_birth date null,
avatar varchar(200) null,
email varchar(200) null,
gender varchar(200) null,
google_auth_id varchar(200) null,
fb_auth_id varchar(200) null,
interested_gender varchar(200) null,
interested_age_min varchar(200) null,
interested_age_max varchar(200) null,
interested_location varchar(200) null,
interested_in varchar(200) null,
phone_number varchar(200) null,
password varchar(200) null,
sign_up_date timestamp,
last_visited timestamp,
online integer
);

drop table if exists message_threads;
create table message_threads(
id serial primary key,
urid varchar(200) unique,
sender_id INTEGER not null REFERENCES users (id),
receiver_id INTEGER not null REFERENCES users (id),
date_created timestamp
);

drop table if exists messages;
create table messages (
id serial primary key,
urid varchar(200) unique,
thread_id INTEGER not null REFERENCES message_threads (id),
sender_id INTEGER not null REFERENCES users (id),
receiver_id INTEGER not null REFERENCES users (id),
message text not null,
date_created timestamp,
date_read timestamp,
read_status integer,
sender_delete int,
receiver_delete int
);

drop table if exists profiles;
create table profiles (
id serial primary key,
urid varchar(200) unique,
user_id integer not null REFERENCES users (id),
kids varchar(200),
smoking varchar(200),
drinking varchar(200),
occupation varchar(200),
state_of_origin varchar(200),
complexion varchar(200),
height_feet integer,
height_inches integer,
about_me text,
school varchar(200),
highest_degree varchar(200)
);

drop table if exists interests;
create table interests (
id serial primary key,
interest varchar(200),
parent_id integer REFERENCES interests (id),
picture varchar(200)
);

drop table if exists user_interests;
create table user_interests (
id serial primary key,
user_id integer REFERENCES users (id),
interest_id integer REFERENCES interests (id)
);

drop table if exists states;
create table states (
id serial primary key,
state varchar(200)
);

drop table if exists pictures;
create table pictures (
id serial primary key,
urid varchar(200) unique,
owner_id INTEGER not null REFERENCES users (id),
filename varchar(200),
description text,
date_uploaded timestamp
);

drop table if exists picture_likes;
create table picture_likes (
id serial primary key,
picture_id INTEGER not null REFERENCES pictures (id),
liker_id INTEGER not null REFERENCES users (id),
date_liked timestamp
);

drop table if exists picture_comments;
create table picture_comments (
id serial primary key,
urid varchar(200) unique,
picture_id INTEGER not null REFERENCES pictures (id),
user_id INTEGER not null REFERENCES users (id),
comment text not null,
date_created timestamp,
visible integer default 0
);

drop table if exists likes;
create table likes(
id serial primary key,
liker_id integer REFERENCES users (id),
like_type integer,
user_id integer REFERENCES users (id),
seen integer,
date_created timestamp
);

drop table if exists location;
create table location(
id serial primary key,
user_id integer REFERENCES users (id),
longitude varchar(200),
latitude varchar(200),
city varchar(200),
state varchar(200),
country varchar(200),
date_updated timestamp
);

drop table if exists blacklist;
create table blacklist(
id serial primary key,
user_id integer REFERENCES users (id),
blocked_id INTEGER REFERENCES users (id),
date_blocked timestamp
);

drop table if exists profile_visits;
create table profile_visits(
id serial primary key,
user_id integer REFERENCES users (id),
visited INTEGER REFERENCES users (id),
date_visited timestamp,
seen integer default 0
);

drop table if exists report_comment;
create table report_comment(
id serial primary key,
user_id integer REFERENCES users (id),
comment_id INTEGER REFERENCES picture_comments (id),
report_text text,
date_reported timestamp
);
