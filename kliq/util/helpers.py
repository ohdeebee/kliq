import uuid

# ensure that id is unique
def genID(model, field):
    uid = uuid.uuid4()
    uniq = uid.hex
    chk = model.query.filter(field == uniq).first()
    if chk:
        genID(model, field)
    else:
        return uniq


# function to return a valid Nigerian phone number in the format 2348*********
def validphone(phone):
    if phone[0:3] == '234':
        #check if next value is 0
        if phone[4] == '0':
            valid = '234' + phone[4:len(phone)]
        else:
            valid = phone
    elif phone[0] == '0':
        valid = '234' + phone[1:len(phone)]
    else:
        valid = '234' + phone

    return valid


# generate random characters for password
def randompwd():
    uid = uuid.uuid4()
    uniq = uid.hex
    return uniq[0:8]