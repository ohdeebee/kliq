# kliq/util/validators.py
from wtforms.validators import ValidationError
from datetime import date, datetime


def numeric(form, field, message=u'This field must be numeric'):
    if not field.data.isdigit():
        raise ValidationError(message)


class Unique(object):
    def __init__(self, model, field, message=u'This element already exists.'):
        self.model = model
        self.field = field
        self.message = message

    def __call__(self, form, field):
        check = self.model.query.filter(self.field == field.data).first()
        if check:
            raise ValidationError(self.message)


class Numeric(object):
    def __init__(self, model, field, message=u'This element must be numeric'):
        self.model = model
        self.field = field
        self.message = message

    def __call__(self, form, field):
        if field.data and not field.data.isdigit():
            raise ValidationError(self.message)


class ValidAge(object):
    def __init__(self, model, field, message=u'You must be at least 18 years old'):
        self.model = model
        self.field = field
        self.message = message

    def __call__(self, form, field):
        today = date.today()
        age = today.year - field.data.year - ((today.month, today.day) < (field.data.month, field.data.day))

        if age < 18:
            raise ValidationError(self.message)