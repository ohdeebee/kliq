from tornado.websocket import WebSocketHandler, WebSocketClosedError
import uuid
import json

# connected users
# connections = {}
conns = {}

class RoomHandler(object):
    """Store data about connections, rooms, which users are in which rooms, etc."""

    def __init__(self):
        self.client_info = {}  # for each client id we'll store  {'wsconn': wsconn, 'room':room, 'nick':nick}
        self.room_info = {}  # dict  to store a list of  {'cid':cid, 'nick':nick , 'wsconn': wsconn} for each room
        self.roomates = {}  # store a set for each room, each contains the connections of the clients in the room.

    def add_roomnick(self, room, nick):
        """Add nick to room. Return generated clientID"""
        pass
        # # meant to be called from the main handler (page where somebody indicates a nickname and a room to join)
        # cid = uuid.uuid4().hex  # generate a client id.
        # if room not in self.room_info:  # it's a new room
        #     self.room_info[room] = []
        # c = 1
        # nn = nick
        # nir = self.nicks_in_room(room)
        # while True:
        #     if nn in nir:
        #         nn = nick + str(c)
        #     else:
        #         break
        #     c += 1

        # self.client_info[cid] = {'room': room, 'nick': nn}  # we still don't know the WS connection for this client
        # self.room_info[room].append({'cid': cid, 'nick': nn})
        # return cid

    # def add_client_wsconn(self, client_id, conn):
    #     """Store the websocket connection corresponding to an existing client."""
    #     self.client_info[client_id]['wsconn'] = conn
    #     cid_room = self.client_info[client_id]['room']
    #
    #     if cid_room in self.roomates:
    #         self.roomates[cid_room].add(conn)
    #     else:
    #         self.roomates[cid_room] = {conn}
    #
    #     for user in self.room_info[cid_room]:
    #         if user['cid'] == client_id:
    #             user['wsconn'] = conn
    #             break
    #     # send "join" and and "nick_list" messages
    #     self.send_join_msg(client_id)
    #     nick_list = self.nicks_in_room(cid_room)
    #     cwsconns = self.roomate_cwsconns(client_id)
    #     self.send_nicks_msg(cwsconns, nick_list)

    def remove_client(self, client_id):
        """Remove all client information from the room handler."""
        cid_room = self.client_info[client_id]['room']

        # first, remove the client connection from the corresponding room in self.roomates
        client_conn = self.client_info[client_id]['wsconn']
        if client_conn in self.roomates[cid_room]:
            self.roomates[cid_room].remove(client_conn)
            if len(self.roomates[cid_room]) == 0:
                del(self.roomates[cid_room])
        # r_cwsconns = self.roomate_cwsconns(client_id)
        # filter out the list of connections r_cwsconns to remove clientID
        # r_cwsconns = [conn for conn in r_cwsconns if conn != self.client_info[client_id]['wsconn']]
        self.client_info[client_id] = None
        for user in self.room_info[cid_room]:
            if user['client_id'] == client_id:
                self.room_info[cid_room].remove(user)
                break
        if len(self.room_info[cid_room]) == 0:  # if room is empty, remove.
            del(self.room_info[cid_room])
            print("Removed empty room %s" % cid_room)


    def nicks_in_room(self, rn):
        """Return a list with the nicknames of the users currently connected to the specified room."""
        nir = []  # nicks in room
        for user in self.room_info[rn]:
            nir.append(user['nick'])
        return nir

    def roomate_cwsconns(self, cid):
        """Return a list with the connections of the users currently connected to the room where
        the specified client (cid) is connected."""
        cid_room = self.client_info[cid]['room']
        # r = {}
        # if cid_room in self.roomates:
        #     r = self.roomates[cid_room]
        print(cid_room)
        return self.room_info[cid_room]

    def send_join_msg(self, client_id):
        """Send a message of type 'join' to all users connected to the room where client_id is connected."""
        nick = self.client_info[client_id]['nick']
        r_cwsconns = self.roomate_cwsconns(client_id)
        msg = {"msgtype": "join", "username": nick, "payload": " joined the chat room."}
        pmessage = json.dumps(msg)
        for conn in r_cwsconns:
            conn.write_message(pmessage)

    @staticmethod
    def send_nicks_msg(conns, nick_list):
        """Send a message of type 'nick_list' (contains a list of nicknames) to all the specified connections."""
        msg = {"msgtype": "nick_list", "payload": nick_list}
        pmessage = json.dumps(msg)
        for c in conns:
            c.write_message(pmessage)

    @staticmethod
    def send_leave_msg(nick, rconns):
        """Send a message of type 'leave', specifying the nickname that is leaving, to all the specified connections."""
        msg = {"msgtype": "leave", "username": nick, "payload": " left the chat room."}
        pmessage = json.dumps(msg)
        for conn in rconns:
            conn.write_message(pmessage)



class WebChat(WebSocketHandler):
    def initialize(self, room_handler):
        """Store a reference to the "external" RoomHandler instance"""
        self.__rh = room_handler

    def open(self, room):
        # generate connection/client id
        client_id = uuid.uuid4().hex
        self.__clientID = client_id

        # check if room exists in room handler
        if room not in self.__rh.room_info:  # it's a new room
            self.__rh.room_info[room] = []

        # add client info to dict and do same for room
        self.__rh.client_info[client_id] = {'room': room, 'client_id': client_id, 'wsconn': self}
        self.__rh.room_info[room].append({'client_id': client_id})

        if room in self.__rh.roomates:
            self.__rh.roomates[room].add(self)
        else:
            self.__rh.roomates[room] = {self}

        # self.__rh.add_client_wsconn(client_id, self)
        print("WebSocket opened. ClientID = %s" % self.__clientID)

    def on_message(self, message):
        # msg = json.loads(message)
        # msg['username'] = self.__rh.client_info[self.__clientID]['nick']
        # pmessage = json.dumps(msg)
        print(message)
        print("backend-on-message handler")
        room = self.__rh.roomate_cwsconns(self.__clientID)
        print(1)
        print(room)
        # for conn in rconns:
        for user in room:
            cid=user['client_id']
            if self.__clientID != cid:
                print(cid)
                print(self.__clientID)
                if self.__rh.client_info[cid]['wsconn']:
                    try:
                        self.__rh.client_info[cid]['wsconn'].write_message(message)
                    except WebSocketClosedError:
                        pass

    def on_close(self):
        print("WebSocket closed")
        self.__rh.remove_client(self.__clientID)


class NotifyHandler(WebSocketHandler):

    def open(self, userid):
        print("notify websocket open")
        # check if connection exists
        global conns
        # if conns.get(userid):
        #     pass
        # else:
        conns[userid] = self
        print(conns)

    def on_message(self, message):
        print("from server")
        self.write_message(message)

    def on_close(self):
        global conns
        for k in conns:
            if k == self:
                del conns[k]
                break
        #conns.remove()
        print("Notification webSocket closed")
        # self.__rh.remove_client(self.__clientID)

    @classmethod
    def receive(cls, userid, message):
        global conns
        print("receive function")
        print(conns)
        ws = conns.get(userid)
        print(ws)
        print(message)
        print(type(ws))
        if ws:
            try:
                print("gets here1")
                ws.write_message(message)
                print("gets here2")
            except WebSocketClosedError:
                del conns[userid]
                print("WS closed error")