from threading import Thread
from kliq import app, mail
from flask_mail import Message
from kliq.util.hurricane import NotifyHandler
from tornado.ioloop import IOLoop
import requests
import json

def send_async_email(app, msg):
    with app.app_context():
        mail.send(msg)

def send_email(subject, email, html_body):
    recipients = [email]
    msg = Message(subject, sender=app.config['MAIL_SENDER'], recipients=recipients)
    msg.html = html_body
    # thr = Thread(target=send_async_email, args=[app, msg])
    # thr.start()

def app_notifications(userid, msgtype, payload=1):
    # print(userid)
    # uid = current_user.urid
    message = json.dumps({'msgtype': msgtype, 'payload': payload})
    IOLoop.instance().add_callback(lambda: NotifyHandler.receive(userid=userid, message=message))

def push_notifications(msg, user, goto):
    # if not user.is_online:
    #send notification
    header = {"Content-Type": "application/json",
      "Authorization": "Basic ZTk4ODljY2ItYWUzOC00MzEzLWFlZWEtN2VkMjAxMzQ2OGUw"}

    payload = {"app_id": "3cfddd11-a2d6-48c6-aabb-53ac7458375c",
               "tags": [{"key": "userid", "relation": "=", "value": user}],
               "contents": {"en": msg},
               "url": goto
               }

    # req = requests.post("https://onesignal.com/api/v1/notifications", headers=header, data=json.dumps(payload))
    #print(req.status_code, req.reason)
