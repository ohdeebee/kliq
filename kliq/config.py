from authomatic.providers import oauth2

DEBUG=True

SECRET_KEY= '3e3fad4f80d81454f0d227108a661ae2'
BCRYPT_LEVEL = 12

# SQLALCHEMY_DATABASE_URI='sqlite:///db/kliq_db.db'
SQLALCHEMY_DATABASE_URI='postgresql+psycopg2://kliquser:kliqlite123@localhost:5432/kliq'
SQLALCHEMY_TRACK_MODIFICATIONS=True

# email server configuration
MAIL_SERVER = 'your.mailserver.com'
MAIL_PORT = 25
MAIL_USERNAME = None
MAIL_PASSWORD = None
MAIL_SENDER = 'no-reply@kliq.com'

UPLOAD_FOLDER = 'uploads'
IMG_UPLOAD_FOLDER = 'images'
AUTH_CONFIG = {
    'google': {
        # Your internal provider name
        # Provider class
        'class_': oauth2.Google,

        # Google is an AuthorizationProvider so we need to set several other properties too:
        'consumer_key': '918263180484-9u0ijohjm8m29igkcjg6jqp4dv12bj3c.apps.googleusercontent.com',
        'consumer_secret': '9YadXd2g8kFde4_Yvz_i1uyC',
        'scope': ['profile', 'email'],
    },
    'facebook': {

        'class_': oauth2.Facebook,

        # Facebook is an AuthorizationProvider too.
        'consumer_key': '271767916522020',
        'consumer_secret': '33a9a3dd4cfb161602a7d4235c2f497c',

        # But it is also an OAuth 2.0 provider and it needs scope.
        'scope': ['user_about_me', 'email', 'public_profile', 'user_birthday'],
    }
}
