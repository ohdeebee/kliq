from kliq import app
from kliq.util.hurricane import WebChat, RoomHandler, NotifyHandler
from tornado.wsgi import WSGIContainer
from tornado.web import Application, FallbackHandler
from tornado.ioloop import IOLoop

# (r"/ws/(.*)", ClientWSConnection, {'room_handler': rh})],
# user_conns = {}
if __name__ == '__main__':
    rh = RoomHandler()
    torn = WSGIContainer(app)
    server = Application([
    	#(r'/websocket/', WebChat),
        (r'/websocket/(.*)', WebChat, {'room_handler': rh}),
        (r'/notification/(.*)', NotifyHandler),
        (r'.*', FallbackHandler, dict(fallback=torn))
    ], debug=True)
    server.listen(5000)
    IOLoop.instance().start()
    #app.run()
